USE [master]
GO
/****** Object:  Database [AthletePerformanceTracker]    Script Date: 05/02/2019 21:06:54 ******/
IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = N'AthletePerformanceTracker')
BEGIN
CREATE DATABASE [AthletePerformanceTracker]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AthletePerformanceTracker', FILENAME = N'D:\SQL2012\Data\AthletePerformanceTracker.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'AthletePerformanceTracker_log', FILENAME = N'D:\SQL2012\Log\AthletePerformanceTracker_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END

GO
ALTER DATABASE [AthletePerformanceTracker] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AthletePerformanceTracker].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AthletePerformanceTracker] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET ARITHABORT OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AthletePerformanceTracker] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AthletePerformanceTracker] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET  DISABLE_BROKER 
GO
ALTER DATABASE [AthletePerformanceTracker] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AthletePerformanceTracker] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET RECOVERY FULL 
GO
ALTER DATABASE [AthletePerformanceTracker] SET  MULTI_USER 
GO
ALTER DATABASE [AthletePerformanceTracker] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AthletePerformanceTracker] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AthletePerformanceTracker] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AthletePerformanceTracker] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'AthletePerformanceTracker', N'ON'
GO
USE [AthletePerformanceTracker]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Test]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Test]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Test](
	[TestId] [bigint] IDENTITY(1,1) NOT NULL,
	[TestMasterId] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_Test_1] PRIMARY KEY CLUSTERED 
(
	[TestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[TestMaster]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TestMaster]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[TestMaster](
	[TestMasterId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED 
(
	[TestMasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[User]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[User]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[User](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[Email] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[UserTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserTest]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserTest](
	[UserTestId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[TestId] [bigint] NOT NULL,
	[Score] [float] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_UserTest] PRIMARY KEY CLUSTERED 
(
	[UserTestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleId], [Name]) VALUES (1, N'Coach')
INSERT [dbo].[Role] ([RoleId], [Name]) VALUES (2, N'Athlete')
SET IDENTITY_INSERT [dbo].[Role] OFF
SET IDENTITY_INSERT [dbo].[TestMaster] ON 

INSERT [dbo].[TestMaster] ([TestMasterId], [Name]) VALUES (1, N'Cooper Test')
INSERT [dbo].[TestMaster] ([TestMasterId], [Name]) VALUES (2, N'Sprint Test')
SET IDENTITY_INSERT [dbo].[TestMaster] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (1, N'Mitchel', N'Fausto', 1, N'Mitchel.Fausto@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (2, N'Queen', N'Jacobi', 2, N'Queen.Jacobi@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (3, N'Magen', N'Faye', 2, N'Magen.Faye@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (4, N'Delicia', N'Ledonne', 2, N'Delicia.Ledonne@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (5, N'Camille', N'Grantham', 2, N'Camille.Grantham@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (6, N'Marc', N'Voth', 2, N'Marc.Voth@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (7, N'Randy', N'Rondon', 2, N'Randy.Rondon@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (8, N'Delora', N'Saville', 2, N'Delora.Saville@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (9, N'Rosario', N'Reuben', 2, N'Rosario.Reuben@test.com', N'login12*')
INSERT [dbo].[User] ([UserId], [FirstName], [LastName], [RoleId], [Email], [Password]) VALUES (10, N'Lula', N'Uhlman', 2, N'Lula.Uhlman@test.com', N'login12*')
SET IDENTITY_INSERT [dbo].[User] OFF
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Test_TestMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[Test]'))
ALTER TABLE [dbo].[Test]  WITH CHECK ADD  CONSTRAINT [FK_Test_TestMaster] FOREIGN KEY([TestMasterId])
REFERENCES [dbo].[TestMaster] ([TestMasterId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Test_TestMaster]') AND parent_object_id = OBJECT_ID(N'[dbo].[Test]'))
ALTER TABLE [dbo].[Test] CHECK CONSTRAINT [FK_Test_TestMaster]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_User_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[User]'))
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserTest_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserTest]'))
ALTER TABLE [dbo].[UserTest]  WITH CHECK ADD  CONSTRAINT [FK_UserTest_User] FOREIGN KEY([TestId])
REFERENCES [dbo].[Test] ([TestId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_UserTest_User]') AND parent_object_id = OBJECT_ID(N'[dbo].[UserTest]'))
ALTER TABLE [dbo].[UserTest] CHECK CONSTRAINT [FK_UserTest_User]
GO
/****** Object:  StoredProcedure [dbo].[AddAthleteInTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AddAthleteInTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[AddAthleteInTest] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure add athlete in paritcular test.
-- =============================================
ALTER PROCEDURE [dbo].[AddAthleteInTest]
	@UserId bigint,
	@TestId bigint,
	@Score nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO UserTest (UserId,TestId,Score,UpdatedOn) VALUES (@UserId,@TestId,@Score,GETDATE())
END

GO
/****** Object:  StoredProcedure [dbo].[CreateTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CreateTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[CreateTest] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure is for creating test for particular date.
-- =============================================
ALTER PROCEDURE [dbo].[CreateTest]
	@TestMasterId int,
	@Date datetime
AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO Test (TestMasterId,Date) VALUES (@TestMasterId,@Date)
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteAthleteFromTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteAthleteFromTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteAthleteFromTest] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure for deleting athlete from test
-- =============================================
ALTER PROCEDURE [dbo].[DeleteAthleteFromTest]
	@UserTestId bigint
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM UserTest WHERE UserTestId = @UserTestId
END

GO
/****** Object:  StoredProcedure [dbo].[DeleteTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DeleteTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[DeleteTest] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure is for deleting test.
-- =============================================
ALTER PROCEDURE [dbo].[DeleteTest]
	@TestId bigint
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM UserTest WHERE TestId = @TestId
	DELETE FROM Test WHERE TestId = @TestId
END

GO
/****** Object:  StoredProcedure [dbo].[GetAllTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAllTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAllTest] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 5/1/2019
-- Description:	This store procedure is for getting all test.
-- =============================================
ALTER PROCEDURE [dbo].[GetAllTest]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT t.TestId,t.Date,tm.Name,(select count(*) from usertest where TestId = t.TestId) AS 'AthleteCount' 
	FROM Test t
    LEFT JOIN TestMaster tm on t.TestMasterId = tm.TestMasterId
	ORDER BY t.Date DESC
    -- Insert statements for procedure here
	
END

GO
/****** Object:  StoredProcedure [dbo].[GetAthleteListForAddNUpdate]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAthleteListForAddNUpdate]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAthleteListForAddNUpdate] AS' 
END
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 5/1/2019
-- Description:	This store procedure for getting athlete list in add and update operation
-- =============================================
ALTER PROCEDURE [dbo].[GetAthleteListForAddNUpdate]
	-- Add the parameters for the stored procedure here
	@AUFlag nvarchar(20),
	@TestId bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@AUFlag = 'add')
	BEGIN
	  SELECT u.UserId,u.FirstName,u.LastName,ISNULL(ut.Score,0) AS Score
	  FROM dbo.[User] u
	  LEFT JOIN UserTest ut on ut.UserId = u.UserId AND ut.TestId = @TestId
	  WHERE u.UserId NOT IN (SELECT UserId FROM UserTest Where TestId = @TestId) AND RoleId != 1
	END
	ELSE
	BEGIN 
	  SELECT u.UserId,u.FirstName,u.LastName,ISNULL(ut.Score,0) AS Score
	  FROM dbo.[User] u
	  INNER JOIN UserTest ut on ut.UserId = u.UserId AND ut.TestId = @TestId
	  WHERE u.UserId IN (SELECT UserId FROM UserTest Where TestId = @TestId) AND RoleId != 1
	END
END

GO
/****** Object:  StoredProcedure [dbo].[GetAthleteResult]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAthleteResult]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetAthleteResult] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure is for getting particular athlete result.
-- =============================================
ALTER PROCEDURE [dbo].[GetAthleteResult]
	@UserId bigint
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ut.UserTestId,tm.Name,t.Date,ut.Score from UserTest ut
    INNER JOIN Test t on ut.TestId = t.TestId
    INNER JOIN TestMaster tm on t.TestMasterId = tm.TestMasterId
    where ut.userid =@UserId
END

GO
/****** Object:  StoredProcedure [dbo].[GetPersonDetail]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPersonDetail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetPersonDetail] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure is for authentication and authorization of user
-- =============================================
ALTER PROCEDURE [dbo].[GetPersonDetail]
	@Email nvarchar(50),
	@Password nvarchar(50)
AS
BEGIN
	SET NOCOUNT ON;


	SELECT * FROM dbo.[User] 
	WHERE Email = @Email and password = @Password
END

GO
/****** Object:  StoredProcedure [dbo].[GetTestAthlete]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTestAthlete]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[GetTestAthlete] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure return athlete list for particular test.
-- =============================================
ALTER PROCEDURE [dbo].[GetTestAthlete] 
@TestId bigint
AS
BEGIN
	SET NOCOUNT ON;
	SELECT ut.UserTestId,ut.TestId,ut.Score,u.UserId,u.FirstName,u.LastName,t.TestMasterId
	FROM UserTest ut
    INNER JOIN dbo.[User] u on ut.UserId = u.UserId
	INNER JOIN Test t on ut.TestId = t.TestId
    WHERE ut.TestId = @TestId ORDER BY ut.Score DESC
END

GO
/****** Object:  StoredProcedure [dbo].[IsTestExist]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[IsTestExist]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[IsTestExist] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This procedure is for checking whether test is already exist or not in database
-- =============================================
ALTER PROCEDURE [dbo].[IsTestExist]
	@TestMasterId int,
	@Date datetime
AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(*) AS 'TestCount' FROM Test WHERE TestMasterId = @TestMasterId AND Date = @Date
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateAthleteInTest]    Script Date: 05/02/2019 21:06:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateAthleteInTest]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[UpdateAthleteInTest] AS' 
END
GO
-- =============================================
-- Create date: 5/1/2019
-- Description:	This store procedure is for updating athlete score in test.
-- =============================================
ALTER PROCEDURE [dbo].[UpdateAthleteInTest]
	@UserId bigint,
	@TestId bigint,
	@Score nvarchar(50)
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE UserTest SET Score = @Score
	WHERE UserId = @UserId AND TestId = @TestId
END

GO
USE [master]
GO
ALTER DATABASE [AthletePerformanceTracker] SET  READ_WRITE 
GO
