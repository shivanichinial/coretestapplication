﻿using AthletePerformanceTrackerAPI.BAL.Model;
using AthletePerformanceTrackerAPI.BAL.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace AthletePerformanceTrackerAPI.Controllers
{

    [RouteAttribute("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TestController : ControllerBase
    {
        private readonly IManageTest _iManageTest;
        public TestController(IManageTest iManageTest)
        {
            _iManageTest = iManageTest;
        }

        /// <summary>
        /// Method for getting all Test
        /// </summary>
        /// <returns>Test list</returns>
        [HttpGet]
        [Route("GetAllTest")]
        public ActionResult<List<GetAllTest>> GetAllTest()
        {
            return _iManageTest.GetAllTest();
        }

        /// <summary>
        /// Method for adding test
        /// </summary>
        /// <param name="test">Model containing Test Details</param>
        /// <returns>Status message</returns>
        [HttpPost]
        [Route("CreateTest")]
        public string CreateTest(AthletePerformanceTrackerAPI.BAL.Model.Test test)
        {
            return _iManageTest.CreateTest(test);
        }

        /// <summary>
        /// Method for removing test
        /// </summary>
        /// <param name="testId"></param>
        /// <returns>Status message</returns>
        [Route("DeleteTest")]
        public string DeleteTest(long testId)
        {
            return _iManageTest.DeleteTest(testId);
        }

        /// <summary>
        /// Method for checking whether test is already exist or not
        /// </summary>
        /// <param name="test"></param>
        /// <returns>Status Message</returns>
        [HttpPost]
        [Route("IsTestExist")]
        public string IsTestExist(AthletePerformanceTrackerAPI.BAL.Model.Test test)
        {
            return _iManageTest.IsTestExist(test);
        }

    }
}
