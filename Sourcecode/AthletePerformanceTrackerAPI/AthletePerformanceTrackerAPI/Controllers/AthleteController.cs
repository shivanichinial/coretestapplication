﻿using AthletePerformanceTrackerAPI.BAL.Model;
using AthletePerformanceTrackerAPI.BAL.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace AthletePerformanceTrackerAPI.Controllers
{
    [RouteAttribute("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AthleteController : ControllerBase
    {
        private IConfiguration _config;
        private readonly IManageAthlete _iManageAthlete;
        public AthleteController(IManageAthlete iManageAthlete, IConfiguration config)
        {
            _iManageAthlete = iManageAthlete;
            _config = config;
        }

        /// <summary>
        /// Method for adding Athlete in test
        /// </summary>
        /// <param name="athleteTest">Model for athlete details</param>
        /// <returns>Status message</returns>
        [HttpPost]
        [Route("AddAthleteInTest")]
        public string AddAthleteInTest(AthleteTest athleteTest)
        {
            return _iManageAthlete.AddAthleteInTest(athleteTest);
        }

        /// <summary>
        /// Method for getting Athlete list for Add and Update functionality
        /// </summary>
        /// <param name="auFlag">Add/Update operation flag</param>
        /// <param name="testId">Testid of a test</param>
        /// <returns>Athlete list</returns>
        [HttpGet]
        [Route("GetAthleteListForAddNUpdate")]
        public List<GetTestAthlete> GetAthleteListForAddNUpdate(string auFlag, long testId)
        {
            return _iManageAthlete.GetAthleteListForAddNUpdate(auFlag, testId);
        }

        /// <summary>
        /// Method for removing Athlete from test
        /// </summary>
        /// <param name="userTestId">Testid of a test</param>
        /// <returns>Status message</returns>
        [HttpDelete]
        [Route("DeleteAthleteFromTest")]
        public string DeleteAthleteFromTest(long userTestId)
        {
            return _iManageAthlete.DeleteAthleteFromTest(userTestId);
        }

        /// <summary>
        /// Method for getting list of Athlete for test.
        /// </summary>
        /// <param name="testId">Testid of a test</param>
        /// <returns>Athlete list</returns>
        [HttpGet]
        [Route("GetTestAthlete")]
        public List<GetTestAthlete> GetTestAthlete(long testId)
        {
            return _iManageAthlete.GetTestAthlete(testId);
        }

        /// <summary>
        /// Method for updating Athlete score for test.
        /// </summary>
        /// <param name="athleteTest">Model for athlete data</param>
        /// <returns>Status message</returns>
        [HttpPost]
        [Route("UpdateAthleteInTest")]
        public string UpdateAthleteInTest(AthleteTest athleteTest)
        {
            return _iManageAthlete.UpdateAthleteInTest(athleteTest);
        }

        /// <summary>
        /// Method for getting results of Athlete
        /// </summary>
        /// <param name="userId">userid of user</param>
        /// <returns>Result list for Athlete</returns>
        [HttpGet]
        [Route("GetAthleteResult")]
        public List<AthleteResult> GetAthleteResult(long userId)
        {
            return _iManageAthlete.GetAthleteResult(userId);
        }
    }
}