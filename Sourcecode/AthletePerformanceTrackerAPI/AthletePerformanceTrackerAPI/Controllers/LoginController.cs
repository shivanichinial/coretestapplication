﻿using AthletePerformanceTrackerAPI.BAL.Model;
using AthletePerformanceTrackerAPI.BAL.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace AthletePerformanceTrackerAPI.Controllers
{
    [RouteAttribute("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private IConfiguration _config;
        private readonly IManageAthlete _iManageAthlete;
        public LoginController(IConfiguration config,IManageAthlete iManageAthlete)
        {
            _config = config;
            _iManageAthlete = iManageAthlete;
        }

        /// <summary>
        /// Method for Authenticating user.
        /// </summary>
        /// <param name="userDetail">Model contains email and password</param>
        /// <returns>User detail</returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetPersonDetail")]
        public IActionResult GetPersonDetail(UserDetail userDetail)
        {
            IActionResult response = Unauthorized();
            UserDetail userReturnDetail = _iManageAthlete.GetPersonDetail(userDetail);
            if (userReturnDetail.Email != null)
            {
                var tokenString = GenerateJSONWebToken(userReturnDetail);
                userReturnDetail.token = tokenString;
                response = Ok(userReturnDetail);
            }
            return response;
        }

        /// <summary>
        /// Method for generating JWT token
        /// </summary>
        /// <param name="userInfo">Model contains email and password</param>
        /// <returns>Token</returns>
        private string GenerateJSONWebToken(UserDetail userInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              null,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}