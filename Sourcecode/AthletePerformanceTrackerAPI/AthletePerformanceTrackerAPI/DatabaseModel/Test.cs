﻿using System;
using System.Collections.Generic;

namespace AthletePerformanceTrackerAPI.DatabaseModel
{
    public partial class Test
    {
        public Test()
        {
            UserTest = new HashSet<UserTest>();
        }

        public long TestId { get; set; }
        public int TestMasterId { get; set; }
        public DateTime Date { get; set; }

        public virtual TestMaster TestMaster { get; set; }
        public virtual ICollection<UserTest> UserTest { get; set; }
    }
}
