﻿using System;
using System.Collections.Generic;

namespace AthletePerformanceTrackerAPI.DatabaseModel
{
    public partial class TestMaster
    {
        public TestMaster()
        {
            Test = new HashSet<Test>();
        }

        public int TestMasterId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Test> Test { get; set; }
    }
}
