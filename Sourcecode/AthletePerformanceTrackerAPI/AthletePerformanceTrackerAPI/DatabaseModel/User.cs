﻿using System;
using System.Collections.Generic;

namespace AthletePerformanceTrackerAPI.DatabaseModel
{
    public partial class User
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}
