﻿using System;
using System.Collections.Generic;

namespace AthletePerformanceTrackerAPI.DatabaseModel
{
    public partial class UserTest
    {
        public long UserTestId { get; set; }
        public long UserId { get; set; }
        public long TestId { get; set; }
        public string Score { get; set; }
        public DateTime UpdatedOn { get; set; }

        public virtual Test Test { get; set; }
    }
}
