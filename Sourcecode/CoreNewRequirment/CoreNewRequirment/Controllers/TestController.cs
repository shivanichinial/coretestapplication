﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CoreNewRequirment.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace CoreNewRequirment.Controllers
{
    public class TestController : Controller
    {
        private string baseURI = string.Empty; 
        private string token = string.Empty;

        private IConfiguration _config;
        public TestController(IConfiguration config)
        {
            _config = config;
            this.baseURI = _config["ApiBaseUrl"];
        }

        #region ManageTest
        public IActionResult TestDetail()
        {
            return View();
        }
        /// <summary>
        ///Method for getting list of test.
        /// </summary>
        /// <returns>Test list</returns>
        [HttpGet]
        public IActionResult Index()
        {
            this.token = HttpContext.Session.GetString("token");
            List<GetAllTest> lstGetAllTest = new List<GetAllTest>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.GetAsync(baseURI + "/Test/GetAllTest");

                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        var readTask = result.Content.ReadAsAsync<GetAllTest[]>();
                        readTask.Wait();

                        var test = readTask.Result;
                        foreach (GetAllTest data in test)
                        {
                            lstGetAllTest.Add(data);
                        }
                    }
                }
                ViewBag.FirstName = HttpContext.Session.GetString("FirstName");
                ViewBag.LastName = HttpContext.Session.GetString("LastName");
                return View("TestDetail", lstGetAllTest);
            }
            catch (Exception)
            {
                return View("TestDetail", lstGetAllTest);
            }
        }
        /// <summary>
        /// Method for creatiion of test.
        /// </summary>
        /// <param name="test">test type,test date</param>
        /// <returns>Status message</returns>
        [HttpGet]
        public string CreateTest(Test test)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    var stringContent = new StringContent(JsonConvert.SerializeObject(test), Encoding.UTF8, "application/json");
                    var token = HttpContext.Session.GetString("token");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.PostAsync(baseURI + "/Test/CreateTest", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        return "Success";
                    }
                }
                return "";
            }
            catch (Exception)
            {
                return "Failure";
            }
        }
        /// <summary>
        /// Method for removing test.
        /// </summary>
        /// <param name="testId">test id</param>
        /// <returns>Status message</returns>
        [HttpGet]
        public string DeleteTest(long testId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.DeleteAsync(baseURI + "/Test/DeleteTest?testId=" + testId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        return readTask.Result.ToString();
                    }
                }
                return "Sucess";
            }
            catch (Exception)
            {
                return "Failure";
            }
        }
        #endregion

        #region ManageAthlete
        /// <summary>
        /// Method for getting result of athlete
        /// </summary>
        /// <returns>Result list of athlete</returns>
        [HttpGet]
        public IActionResult GetAthleteResultList()
        {
            List<AthleteResult> lstAthleteResult = new List<AthleteResult>();
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.GetAsync(baseURI + "/Athlete/GetAthleteResult?userId=" + HttpContext.Session.GetInt32("UserId"));
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        var readTask = result.Content.ReadAsAsync<AthleteResult[]>();
                        readTask.Wait();

                        var test = readTask.Result;
                        foreach (AthleteResult data in test)
                        {
                            if(data.TestName.Contains("Sprint"))
                            {
                                
                            }
                            lstAthleteResult.Add(data);
                        }
                    }
                }
                ViewBag.FirstName = HttpContext.Session.GetString("FirstName");
                ViewBag.LastName = HttpContext.Session.GetString("LastName");
                return View(lstAthleteResult);
            }
            catch(Exception)
            {
                ViewBag.FirstName = HttpContext.Session.GetString("FirstName");
                ViewBag.LastName = HttpContext.Session.GetString("LastName");
                return View(lstAthleteResult);
            }
        }

        /// <summary>
        /// Method for getting athlete list for add and update of athlete in test
        /// </summary>
        /// <param name="auFlag">Flag for Add/Update operation</param>
        /// <param name="testId">test id</param>
        /// <returns>Athlete list for test</returns>
        [HttpGet]
        public List<GetTestAthlete> GetAthleteForTest(string auFlag, long testId)
        {
            List<GetTestAthlete> lstGetTestAthlete = new List<GetTestAthlete>();
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.GetAsync(baseURI + "/Athlete/GetAthleteListForAddNUpdate?auFlag=" + auFlag + "&testId=" + testId);

                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsAsync<GetTestAthlete[]>();
                        readTask.Wait();
                        var data = readTask.Result;
                        foreach (GetTestAthlete testAthlete in data)
                        {
                            lstGetTestAthlete.Add(testAthlete);
                        }
                    }
                }
                return lstGetTestAthlete;
            }
            catch (Exception)
            {
                return lstGetTestAthlete;
            }
        }

        /// <summary>
        /// Method for updating athlete score in test
        /// </summary>
        /// <param name="athleteTest">test id,user id and updated score</param>
        /// <returns>Status message</returns>
        [HttpPost]
        public string UpdateAthleteInTest(AthleteTest athleteTest)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    var stringContent = new StringContent(JsonConvert.SerializeObject(athleteTest), Encoding.UTF8, "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.PostAsync(baseURI + "/Athlete/UpdateAthleteInTest", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        return readTask.Result;
                    }
                    else
                    {
                        return "Failure";
                    }
                }
            }
            catch (Exception)
            {
                return "Failure";
            }
        }

        /// <summary>
        /// Method for adding athlete in test
        /// </summary>
        /// <param name="athleteTest">test id,user id and score</param>
        /// <returns>Status message</returns>
        [HttpPost]
        public string AddAthleteInTest(AthleteTest athleteTest)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    var stringContent = new StringContent(JsonConvert.SerializeObject(athleteTest), Encoding.UTF8, "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.PostAsync(baseURI + "/Athlete/AddAthleteInTest", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        return readTask.Result;
                    }
                    else
                    {
                        return "Failure";
                    }
                }
            }
            catch (Exception)
            {
                return "Failure";
            }
        }

        /// <summary>
        /// Method for checking whether test exist or not
        /// </summary>
        /// <param name="test">test type and date</param>
        /// <returns>Status message</returns>
        [HttpGet]
        public string IsTestExist(Test test)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    var stringContent = new StringContent(JsonConvert.SerializeObject(test), Encoding.UTF8, "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.PostAsync(baseURI + "/Test/IsTestExist", stringContent);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        return readTask.Result.ToString();
                    }
                }
                return "Sucess";
            }
            catch (Exception)
            {
                return "Failure";
            }
        }

        /// <summary>
        /// Method for getting athlete list of particular test
        /// </summary>
        /// <param name="testid"></param>
        /// <param name="testname"></param>
        /// <param name="date"></param>
        /// <param name="TestId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetTestAthlete(long testid, string testname, DateTime date, long TestId)
        {
            try
            {
                List<GetTestAthlete> lstGetTestAthlete = new List<GetTestAthlete>();
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.GetAsync(baseURI + "/Athlete/GetTestAthlete?testId=" + testid);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        var readTask = result.Content.ReadAsAsync<GetTestAthlete[]>();
                        readTask.Wait();

                        var test = readTask.Result;
                        foreach (GetTestAthlete data in test.OrderByDescending(x => x.Score))
                        {
                            if (data.TestMasterId == 1)
                            {
                                if (Convert.ToInt16(data.Score) <= 1000)
                                    data.FitnessRating = "Below Average";
                                else if (Convert.ToInt16(data.Score) > 1000 && Convert.ToInt16(data.Score) <= 2000)
                                    data.FitnessRating = "Average";
                                else if (Convert.ToInt16(data.Score) > 2000 && Convert.ToInt16(data.Score) <= 3500)
                                    data.FitnessRating = "Good";
                                else if (Convert.ToInt16(data.Score) > 1000)
                                    data.FitnessRating = "Very good";
                                lstGetTestAthlete.Add(data);
                            }
                            else if (data.TestMasterId == 2)
                            {
                                if (Convert.ToInt16(data.Score) >= 12)
                                    data.FitnessRating = "Very good";
                                else if (Convert.ToInt16(data.Score) > 10 && Convert.ToInt16(data.Score) < 12)
                                    data.FitnessRating = "Good";
                                else if (Convert.ToInt16(data.Score) > 9 && Convert.ToInt16(data.Score) <= 10)
                                    data.FitnessRating = "Average";
                                else if (Convert.ToInt16(data.Score) <= 9)
                                    data.FitnessRating = "Below average";
                                lstGetTestAthlete.Add(data);
                            }
                        }
                    }
                }
                ViewBag.TestName = testname + " D. " + date.ToShortDateString();
                ViewBag.TestId = TestId;
                ViewBag.FirstName = HttpContext.Session.GetString("FirstName");
                ViewBag.LastName = HttpContext.Session.GetString("LastName");
                return View("TestAthleteDetail", lstGetTestAthlete);
            }
            catch (Exception)
            {
                ViewBag.FirstName = HttpContext.Session.GetString("FirstName");
                ViewBag.LastName = HttpContext.Session.GetString("LastName");
                return View("TestAthleteDetail", new List<GetTestAthlete>());
            }
        }

        /// <summary>
        /// Method for deleting athlete from a test.
        /// </summary>
        /// <param name="userTestId"></param>
        /// <returns>Status Message</returns>
        [HttpGet]
        public string DeleteAthleteFromTest(long userTestId)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    this.token = HttpContext.Session.GetString("token");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + this.token);
                    var responseTask = client.DeleteAsync(baseURI + "/Athlete/DeleteAthleteFromTest?userTestId=" + userTestId);
                    responseTask.Wait();

                    var result = responseTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var readTask = result.Content.ReadAsStringAsync();
                        readTask.Wait();
                        return readTask.Result.ToString();
                    }
                    else
                    {
                        return "Failure";
                    }
                }
            }
            catch (Exception)
            {
                return "Failure";
            }
        }
        #endregion
    }
}