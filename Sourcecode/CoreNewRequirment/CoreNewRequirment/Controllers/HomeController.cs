﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CoreNewRequirment.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;

namespace CoreNewRequirment.Controllers
{
    public class HomeController : Controller
    {
        public string baseURI = string.Empty;

        private IConfiguration _config;
        public HomeController(IConfiguration config)
        {
            _config = config;
            this.baseURI = _config["ApiBaseUrl"];
        }

        /// <summary>
        /// Get action for login view
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Login()
        {
            UserDetail userDetail = new UserDetail();
            return View(userDetail);
        }

        /// <summary>
        /// Post action for login view
        /// </summary>
        /// <param name="userDetail">Model for email and password</param>
        /// <returns>View</returns>
        [HttpPost]
        public ActionResult Login(UserDetail userDetail)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var client = new HttpClient())
                    {
                        userDetail.Email = userDetail.Email.Trim();
                        userDetail.Password = userDetail.Password.Trim();
                        var stringContent = new StringContent(JsonConvert.SerializeObject(userDetail), Encoding.UTF8, "application/json");
                        var responseTask = client.PostAsync(baseURI + "/Login/GetPersonDetail", stringContent);
                        responseTask.Wait();

                        var result = responseTask.Result;
                        if(result.ReasonPhrase.ToLower() =="unauthorized")
                        {
                            ViewBag.ValidationMessage = "Please enter valid email address and password";
                        }
                        if (result.IsSuccessStatusCode)
                        {
                            var readTask = result.Content.ReadAsAsync<UserDetail>();
                            readTask.Wait();
                           userDetail = readTask.Result;
                            
                            HttpContext.Session.SetInt32("UserId", Convert.ToInt32(userDetail.UserId));
                            HttpContext.Session.SetInt32("RoleId", Convert.ToInt32(userDetail.RoleId));
                            HttpContext.Session.SetString("FirstName", Convert.ToString(userDetail.FirstName));
                            HttpContext.Session.SetString("LastName", Convert.ToString(userDetail.LastName));
                            HttpContext.Session.SetString("token", userDetail.token);
                                if(userDetail.RoleId == 1)
                                { 
                                  return RedirectToAction("Index", "Test");
                                }
                                else if(userDetail.RoleId == 2)
                                {
                                    return RedirectToAction("GetAthleteResultList", "Test");
                                }
                          
                        }
                    }
                }
                return View(userDetail);
            }
            catch (Exception ex)
            {
                return View(userDetail);
            }
        }

        /// <summary>
        /// Get action for logout view
        /// </summary>
        /// <returns>View</returns>
        [HttpGet]
        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }
    }
}
