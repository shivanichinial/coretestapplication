﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreNewRequirment.Models
{
    /// <summary>
    /// Model for all data related to Test and Athlete
    /// </summary>
    public class GetTestAthlete
    {
        public long UserTestId { get; set; }
        public long TestId { get; set; }
        public Int16 TestMasterId { get; set; }
        public float Score { get; set; }
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FitnessRating { get; set; }
    }

    /// <summary>
    /// Model for data related to Athlete score
    /// </summary>
    public class AthleteTest
    {
        public long UserId { get; set; }
        public long TestId { get; set; }
        public decimal Score { get; set; }
    }

    /// <summary>
    /// Model for data related to Athlete test
    /// </summary>
    public class AthleteResult
    {
        public long UserTestId { get; set; }
        public string TestName { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime Date { get; set; }
        public decimal Score { get; set; }
    }
}
