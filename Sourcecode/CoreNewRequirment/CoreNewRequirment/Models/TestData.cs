﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreNewRequirment.Models
{
    /// <summary>
    /// Model for overall data of Test
    /// </summary>
    public class TestData
    {
        public DateTime date { get; set; }
        public int NumberofParticipants{ get; set; }
        public string TestType { get; set; }
        public string Distance { get; set; }
        public string Duration { get; set; }
    }

    /// <summary>
    /// Model for Test name and date details
    /// </summary>
    public class Test
    {
        public long TestId { get; set; }
        public long TestMasterId { get; set; }
        public DateTime Date { get; set; }
    }

    /// <summary>
    /// Model for data related to its Test and Athlete count.
    /// </summary>
    public class GetAllTest
    {
        public long TestId { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public int AthleteCount { get; set; }
    }

    /// <summary>
    /// Model for details of User.
    /// </summary>
    public class UserDetail
    {
        public long UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long RoleId { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        public string token { get; set; }
    }
}
